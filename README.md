# Aragonite, the interpreted language [![License](https://img.shields.io/gitlab/license/EgorChernov/aragonite)](/LICENSE) ![Stars](https://img.shields.io/gitlab/stars/EgorChernov%2Faragonite) ![Version](https://img.shields.io/badge/version-8.4--stable-blue)
That programming language was published as **public** at 03/01/2023. This is interpreted programming language which meaning that source code **can run as is**

(C) Egor Chernov. All rights reversed.
## How to use?
### Linux
- Open terminal using <kbd>Ctrl</kbd> <kbd>Alt</kbd> <kbd>T</kbd> and type in prompt:
    
    `Ubuntu:`
    ```sh
    sudo apt-get install git
    git clone https://gitlab.com/EgorChernov/aragonite; cd aragonite; pip install -r requirements.txt
    ```
    Or else
    ```sh
    sudo apt install git
    git clone https://gitlab.com/EgorChernov/aragonite; cd aragonite; pip install -r requirements.txt
    ```
- Go to directory of aragonite
- Type in prompt
    ```sh
    python3 shell.py
    ```
- Shell should appear and you able to write commands in it.
### Windows
#### Python installation
- Download Python installer at official [website](https://python.org/)
- Install Python following this tutorial

[<img width="350" alt="gitlab_tutorial_windows.png" src="img/gitlab_tutorial_windows.png">](img/gitlab_tutorial_windows.png)

[<img width="350" alt="gitlab_tutorial_windows2.png" src="img/gitlab_tutorial_windows2.png">](img/gitlab_tutorial_windows2.png)

[<img width="350" alt="gitlab_tutorial_windows3.png" src="img/gitlab_tutorial_windows3.png">](img/gitlab_tutorial_windows3.png)

[<img width="350" alt="gitlab_tutorial_windows4.png" src="img/gitlab_tutorial_windows4.png">](img/gitlab_tutorial_windows4.png)
#### Install Aragonite
- Install [git](https://git-scm.com/)
##### Git
- Run this command in command prompt.
    ```bat
    git clone https://gitlab.com/EgorChernov/aragonite; cd aragonite; pip install -r requirements.txt
    ```
- Folder `aragonite` should appear in current directory. Open it, try to run `shell.py` it must run and give you a shell.

##### Gitlab
- Go to [download link](https://gitlab.com/EgorChernov/aragonite/-/archive/main/aragonite.zip)
- Unzip that file. Open folder with files, try to run `shell.py` it must run and give you a shell.
#### Run Aragonite
- Press <kbd>Win</kbd> <kbd>R</kbd>
- Type "cmd"
- Press <kbd>Enter</kbd>
- Go to directory of Downloads and unzip your downloaded source code of Aragonite
- Go to directory of unzipped source code of Aragonite
- Type in command prompt 
    ```bat
    python shell.py
    ```
## Questions nobody ask
Q: Is this software proprietary?

A: No, It's free to use and to change! :)
